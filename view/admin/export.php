<?php
session_start();
  if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = $_GET['id'];
}else{
    $id = 0;
}


  ?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Luuma: export.php</title>

    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">
    <style>
      .imageService{
            display: block;
            margin: auto;
            width: 800px;
        }
    </style>
  </head>

  <body>
    <section id="container">
     
      <header class="header black-bg">
        <div class="sidebar-toggle-box">
          <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="#" class="logo"><b>Luuma</b></a>
        <!--logo end-->
        <div class="top-menu">
          <ul class="nav pull-right top-menu">
            <li>
              <a class="logout" href="login.html">Se déconnecter</a>
            </li>
          </ul>
        </div>
      </header>
     
      <aside>
        <div id="sidebar" class="nav-collapse ">
          <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">
            <p class="centered">
              <a href="profile.html"><img src="img/ui-sam.jpg" class="img-circle" width="80"></a>
            </p>
            <h5 class="centered"><?= $_SESSION['prenomPersonne'] . " " ?><?= $_SESSION['nomPersonne'] ?></h5>

            <li class="mt">
              <a href="fichierIndex.php">
                <i class="fa fa-dashboard"></i>
                <span>Dashboard</span>
              </a>
            </li>
            <li class="sub-menu">
              <a href="admin.php">
                <i class="fa fa-desktop"></i>
                <span>Administrateur</span>
              </a>
            </li>
            <li class="sub-menu">

               <a href="../../controller/fichierController.php?retourService=<?=$_GET['idService']?>">
                <i class="fa fa-desktop"></i>
                <span>Service</span>
              </a>
          
            </li>
            <li class="sub-menu">
              <a href="typeService.php">
                <i class="fa fa-cogs"></i>
                <span>Type Services</span>
              </a>
          </ul>
        </div>
      </aside>
      
      <section id="main-content" class="mt-10">
        <section class="wrapper">
          <br>
            <img class="" src="exportImage.php?id=<?=$id?>" alt="">
        </section>
      </section>
    
      <footer class="site-footer">
        <div class="text-center">
          <p>
            &copy; Copyrights <strong>Luuma</strong>.
          </p>
        </div>
      </footer>
    </section>
    <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
    <script src="lib/jquery.scrollTo.min.js"></script>
    <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="lib/common-scripts.js"></script>

  </body>

  </html>