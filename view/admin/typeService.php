<?php
  session_start();
  require_once '../../bd/Connection.php';
  function chargerClass($class){
    require ('../../model/'. $class. '.php');
  }    
  spl_autoload_register('chargerClass');

    $serviceManager = new TypeServiceManager(getConnection());
    $tabTypeService = $serviceManager->Lister();
    // var_dump($tabTypeService); die();
    if(isset($_GET['modifierType']) && !empty($_GET['modifierType'])){
      $_SESSION['modifierType'] = $_SESSION['idType'];
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Luuma:typeService.php</title>



  <!-- Bootstrap core CSS -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

</head>

<body>
  <section id="container" class="mt-12">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.html" class="logo"><b>Luuma</b></a>
      <!--logo end-->
  
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="login.html">Se Deconnecter</a></li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="#"><img src="img/ui-sam.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered"><?=$_SESSION['prenomPersonne']. " "?><?=$_SESSION['nomPersonne']?></h5>

          <li class="mt">
            <a href="fichierIndex.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
          
          <li class="sub-menu">
            <a href="admin.php">
              <i class="fa fa-cogs"></i>
              <span>Administrateur</span>
              </a>
           
          </li>
          <li class="sub-menu">
            <a href="service.php">
              <i class="fa fa-book"></i>
              <span>Service</span>
              </a>
            
          </li>
          <li class="sub-menu">
            <a href="#">
              <i class="fa fa-tasks"></i>
              <span>Type Service</span>
              </a>
          </li>
        
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
   
    <section id="main-content">
      <section class="wrapper">
        <div class="row mt">
          <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="showback">
            <h5><i class="fa fa-angle-right"></i> Ajouter / Modifier Catégorie Service</h5><hr>
            <form  class="form-horizontal style-form" method="POST" action="../../controller/fichierController.php">
                <div class="form-group">
                  <label class="col-sm-12 col-sm-12 control-label">Libelle</label>
                  <div class="col-sm-12">
                    <?php
                     if(isset($_GET['modifierType']) && !empty($_GET['modifierType'])){
                    ?>
                    <input name="libelle" type="text" class="form-control" value="<?=$_SESSION['libelle']?>" required>
                    <?php
                     }else{
                    ?>
                    <input name="libelle" type="text" class="form-control" required>
                    <?php
                     }
                    ?>
                  </div>
                </div>
                  <div class="form-group">
                    <label class="col-sm-12 col-sm-12 control-label">Description</label>
                    <div class="col-sm-12">
                        <?php
                        if(isset($_GET['modifierType']) && !empty($_GET['modifierType']))
                        {
                        ?>
                        <textarea name="description" id="" cols="26" rows="8"><?=$_SESSION['description']?></textarea>
                        <?php
                        }else{
                        ?>
                        <textarea name="description" id="" cols="26" rows="8"></textarea>

                        <?php
                        }
                        ?>
            
                    </div>
                  </div>
                  <div class="form-group">
                  <div class="text-center">
                  <?php
                        if(isset($_GET['modifierType']) && !empty($_GET['modifierType']))
                        {
                        ?>
                          <button name="ModifierTypeService" class="btn btn-primary btn-xl text-uppercase" id="submitButton" type="submit">Enrégistrer</button>
                        <?php
                        }else{
                        ?>
                          <button name="enregistrerTypeService" class="btn btn-primary btn-xl text-uppercase" id="submitButton" type="submit">Enrégistrer</button>

                        <?php
                        }
                        ?>
                    </div>
                  </div>
                </form>
            </div>
  
          </div>
          <!-- /col-lg-6 -->
          <div class="col-lg-9 col-md-9 col-sm-12">
            <!--  ALERTS EXAMPLES -->
            <div class="showback">
              <h4><i class="fa fa-angle-right"></i> Liste des Catégories de Sevice</h4>
              <table class="table table-striped table-advance table-hover table-bordered table-condensed">
                <hr>
                <thead>
                  <tr>
                    <th><i class="fa fa-bookmark"></i> Id</th>
                    <th><i class="fa fa-bookmark"></i> Libelle</th>
                    <th><i class="fa fa-bullhorn"></i> Description</th>
                    <th><i class="fa fa-bullhorn"></i> Etat</th>
                    <th><i class="fa fa-bullhorn"></i> Date Enrégistrer</th>
                    <th><i class="fa fa-bullhorn"></i> Date de Modification</th>  
                    <th><i class="fa fa-bullhorn"></i> Actions</th>  
                  </tr>
                </thead>
                <tbody>
                <?php
                     foreach($tabTypeService as $service){
                      // var_dump($ad);
                      ?>
                      <tr>
                        <td><?=$service->getId()?></td>
                        <td><?=$service->getLibelle()?></td>
                        <td><?=$service->getDescription()?></td>
                        <td><?=$service->getEtat()?></td>
                        <td><?=$service->getDateEnregistrer()?></td>
                        <td><?=$service->getDateModification()?></td>
                        <td>
                          <a href="../../controller/fichierController.php?idActiverType=<?=$service->getId()?>" class="btn btn-success btn-xs"><i class="fa fa-check"></i></a>
                        <!-- <button type="submit"  class="btn btn-success btn-xs"><i class="fa fa-check"></i></button> -->
                          <a href="../../controller/fichierController.php?idModifierType=<?=$service->getId()?>&amp;etatType=<?=$service->getEtat()?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                        <!-- <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button> -->
                          <a href="../../controller/fichierController.php?idDesactiverType=<?=$service->getId()?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                        <!-- <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button> -->
                        </td>
                      </tr>
                  <?php
                     }
                  ?>
                </tbody>
              </table>
            <!-- /content-panel -->
            </div>
        
            
          </div>
          <!-- /col-lg-6 -->
        </div>
        <!--/ row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Luuma</strong>. 
        </p>
       
        <a href="general.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>

  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript" src="lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="lib/gritter-conf.js"></script>
</body>

</html>
