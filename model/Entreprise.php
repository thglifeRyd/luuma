<?php
    class Entreprise {
        private $_nomEntreprise;
        private $_description;
        private $_idAdministrateur;
        private $_telephone;
        private $_adresse;
        private $_logo;


        public function __construct(array $donne)
        {
            $this->hydratation($donne);
        }
        public function setLogo($logo){
            $this->_logo = $logo;
        }
        public function getLogo(){
            return $this->_logo;
        }
        public function setNomEntreprise($nom){
            $this->_nomEntreprise = $nom;
        }
        public function setDescription($desc){
            $this->_description = $desc;
        }
        public function setIdAdministrateur($id){
           
                $this->_idAdministrateur = $id;
            
        }
        public function getNomEntreprise(){
            return $this->_nomEntreprise;
        }
        public function getIdAdministrateur(){
            return $this->_idAdministrateur;
        }
        public function getDescription(){
            return $this->_description;
        }

        public function getTelephone(){
            return $this->_telephone;
        }
        public function getAdresse(){
            return $this->_adresse;
        }
        public function setTelephone($tel){
          
                $this->_telephone = $tel;
            
        }
        public function setAdresse($add){
            $this->_adresse = $add;
        }
        public function hydratation(array $donne)
        {
           foreach($donne as $key => $value){
               $method = 'set'. ucfirst($key);
               if(method_exists($this, $method)){
                   $this->$method($value);
               }
           }
        }
        
      
    }
?>