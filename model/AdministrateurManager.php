<?php

    class AdministrateurManager {
        private $_bd;
        
        public function __construct($bd){
            $this->setBd($bd);
        } 
        private function setBd($bd){
            $this->_bd = $bd;
        }


        public function Exists($pseudo, $password){//Vérifie l'existence de l'Administrateur
            $requete = $this->_bd->prepare('SELECT * FROM Administrateur 
                                        where login = :nom and motPasse = :pass And etat = :etat');
            $requete->execute(array(
                ':nom' => $pseudo,
                ':pass' => $password,
                ':etat' => 1
            ));
            return $requete->fetch(2);
        }
        public function Supprimer(int $id)
        {
            $requete = $this->_bd->prepare("DELETE FROM Administrateur WHERE id = :id");

            $requete->bindValue(':id', $id, PDO::PARAM_INT);
            $requete->execute();
            return true;
        }

        // --------------------------------------------------------------
        public function Ajouter(Administrateur $admin)
        {
            $requet = $this->_bd->prepare('INSERT INTO Administrateur set nom = :nom, prenom = :prenom,
                telephone = :tel, email = :email, login = :log, motPasse =:passe, dateEnregistrer = now() 
            ');

            $requet->execute(array(
                ':nom' => $admin->getNom(),
                ':prenom' => $admin->getPrenom(),
                ':tel' => $admin->getTelephone(),
                ':email' => $admin->getEmail(),
                ':log' => $admin->getLogin(),
                ':passe' => $admin->getMotPasse()
            ));   
            return true;
        }
        public function Modifier(Administrateur $admin, $id)
        {
            
            $requet = $this->_bd->prepare('UPDATE Administrateur SET nom = :nom, prenom = :prenom,
            telephone = :tel, email = :email, login = :log, motPasse = :passe, dateModification = now() WHERE id = :id');
            
            $requet->execute(array(
                ':nom' => $admin->getNom(),
                ':prenom' => $admin->getPrenom(),
                ':tel' => $admin->getTelephone(),
                ':email' => $admin->getEmail(),
                ':passe' => $admin->getMotPasse(),
                ':log' => $admin->getLogin(),
                ':id' => (int)$id
            ));
            return true;
        }
      
        public function Lister()
        {
            $requet = $this->_bd->query('SELECT * FROM Administrateur');

            $Administrateurs = array();
            while($donne = $requet->fetch(PDO::FETCH_ASSOC)){
                $Administrateurs[] = new Administrateur($donne);
            }
            
            return $Administrateurs;
        }
        public function SelectAdmin(int $id){
            $requet = $this->_bd->prepare('SELECT * from Administrateur WHERE id = :id AND etat = 1');
            $requet->bindValue(':id', $id, PDO::PARAM_INT);
            $requet->execute();
            // var_dump($requet->fetch(PDO::FETCH_ASSOC)); die();
            return new Administrateur($requet->fetch(PDO::FETCH_ASSOC));
        }
        public function Activer(int $id)
        {
            $requet = $this->_bd->prepare('UPDATE Administrateur SET etat = :etat, dateModification = NOW() WHERE id = :id');
            $requet->execute(array(
                ':etat' => 1,
                ':id' => (int)$id
            ));
            return true;
        }
        public function Desactiver(int $id)
        {
            $requet = $this->_bd->prepare('UPDATE Administrateur SET etat = :etat, dateModification = NOW() WHERE id = :id');
            $requet->execute(array(
                ':etat' => 0,
                ':id' => (int)$id
            ));
            return true;
        }
        public function AjouterContactUtilisateur(Administrateur $admin)
        {
            $requet = $this->_bd->prepare('INSERT INTO Contact_utilisateur set nomPrenom = :nom, message = :sms,
                telephone = :tel, email = :email, dateEnregistrer = now() 
            ');

            $requet->execute(array(
                ':nom' => $admin->getNomPrenom(),
                ':sms' => $admin->getMessage(),
                ':tel' => $admin->getTelephone(),
                ':email' => $admin->getEmail()
            ));   
            return true;
        }
        public function ListerContact(){
            $requet = $this->_bd->query('SELECT * FROM Contact_utilisateur');

            $Administrateurs = array();
            while($donne = $requet->fetch(PDO::FETCH_ASSOC)){
                $Administrateurs[] = new Administrateur($donne);
            }
            
            return $Administrateurs;   
        }

        public function SupprimerContact(int $id)
        {
            $requet = $this->_bd->prepare('DELETE FROM Contact_utilisateur WHERE id = :id');
            $requet->execute(array(
                ':id' => (int)$id
            ));
            return true;
        }
    }
   
?>