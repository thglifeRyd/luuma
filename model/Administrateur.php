<?php
    class Administrateur {
        private $_id;
        private $_nom;
        private $_prenom;
        private $_telephone;
        private $_email;
        private $_login;
        private $_motPasse;
        private $_dateEnregistrer;
        private $_dateModification;
        private $_etat;
        private $_nomPrenom;
        private $_message;

        public function __construct(array $donne)
        {
            $this->hydratation($donne);
        }
        public function setId($id){
            
                $this->_id = $id;
            
        }
        public function setNomPrenom($nom){
            $this->_nomPrenom = $nom;
        }
        public function setMessage($nom){
            $this->_message = $nom;
        }
        public function getMessage(){
            return $this->_message;
        }
        public function getNomPrenom(){
            return $this->_nomPrenom;
        }
        public function setNom($nom){
            $this->_nom = $nom;
        }
        public function setPrenom($prenom){
            $this->_prenom = $prenom;
        }
        public function setTelephone($tel){
           
                $this->_telephone = $tel;
            
        }
        public function setEmail($email){
            $this->_email = $email;
        }
        public function getId(){
            return $this->_id;
        }
        public function getNom(){
            return $this->_nom;
        }
        public function getPrenom(){
            return $this->_prenom;
        }
        public function getTelephone(){
            return $this->_telephone;
        }
        public function getEmail(){
            return $this->_email;
        }
        public function getLogin(){
            return $this->_login;
        }
        public function getMotPasse(){
            return $this->_motPasse;
        }
        public function setLogin($login){
            $this->_login = $login;
        }
        public function setMotPasse($mot){
            $this->_motPasse = $mot;
        }
        public function setDateEnregistrer($date){
            $this->_dateEnregistrer = $date;
        }
        public function setDateModification($date){
            $this->_dateModification = $date;
        }
        public function getDateEnregistrer(){
            return $this->_dateEnregistrer;
        }
        public function getDateModification(){
            return $this->_dateModification;
        }
        public function getEtat(){
            return $this->_etat;
        }
        public function setEtat($etat){
       
                $this->_etat = $etat;
            
        }
        

        public function hydratation(array $donne)
        {
           foreach($donne as $key => $value){
               $method = 'set'. ucfirst($key);
               if(method_exists($this, $method)){
                   $this->$method($value);
               }
           }
        }
    }
?>