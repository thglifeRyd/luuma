<?php
    abstract class Crud{
        public abstract function Ajouter(Object $object);
        public abstract function Modifier(Object $object);
        public abstract function Lister();
        public abstract function Desactiver(int $id);
        public abstract function Activer(int $id);
    }
?>