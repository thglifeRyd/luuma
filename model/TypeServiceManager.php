<?php
session_start();
    class TypeServiceManager {
        private $_bd;
        public function __construct($bd)
        {
            $this->setBd($bd);
        }
        private function setBd($bd){
            $this->_bd = $bd;
        }

        //..................................................
        public function Ajouter(TypeService $type)
        {
            $requet = $this->_bd->prepare('INSERT TypeService set libelle = :lib, description =:desc, dateEnregistrer = now()
            ');

            $requet->execute(array(
                ':lib' => $type->getLibelle(),
                ':desc' => $type->getDescription()
            ));   
            return true;
        }
        public function  Modifier(TypeService $type)
        {
            // var_dump( $type->getDescription ()); die();
            $requet = $this->_bd->prepare('UPDATE TypeService SET libelle = :lib, description = :desc, dateModification = now() WHERE id = :id
                        ');
            $requet->execute(array(
                ':lib' => $type->getLibelle(),
                ':desc' => $type->getDescription(),
                ':id' => (int)$_SESSION['modifierType']
            ));
            return true;
        }
        public function Lister()
        {
            $requet = $this->_bd->query('SELECT * FROM TypeService where etat = 1');

            $types = array();
            while($donne = $requet->fetch(PDO::FETCH_ASSOC)){
                $types[] = new TypeService($donne);
            }
            return $types;
        }
        // _________________________________________________
        public function Activer(int $id)
        {
           
            $requet = $this->_bd->prepare("UPDATE TypeService SET etat = :etat, dateModification = NOW() WHERE id = :id");
            $requet->bindValue(':etat', 1, PDO::PARAM_INT);
            $requet->bindValue(':id', $id, PDO::PARAM_INT);
            $requet->execute();
            return true;
        }
        public function Desactiver(int $id)
        {
            $requet = $this->_bd->prepare("UPDATE TypeService SET etat = :etat, dateModification = NOW() WHERE id = :id");
            $requet->bindValue(':etat', 0, PDO::PARAM_INT);
            $requet->bindValue(':id', $id, PDO::PARAM_INT);
            $requet->execute();
            return true;
        }
        public function SelectTypeService(int $id){
            $requet = $this->_bd->prepare('SELECT * from TypeService WHERE id = :id AND etat = 1');
            $requet->bindValue(':id', $id, PDO::PARAM_INT);
            $requet->execute();
          
            return new TypeService($requet->fetch(PDO::FETCH_ASSOC));
        }
      
    }
?>