<?php
    require_once "bd/Connection.php";
    function chargerClass($class)
    {
      require('model/' . $class . '.php');
    }
    spl_autoload_register('chargerClass');

    $serviceManager = new ServiceManager(getConnection());
    $tabService = $serviceManager->SelectServices();
    $tabImage = $serviceManager->selectImage();
    $entrepriseManager = new EntrepriseManager(getConnection());
    $entreprise = $entrepriseManager->AfficherEntreprise();
    // var_dump($tabImage); die();
    // var_dump($tabService); die();
    //fonction qui cherche les images 
    if(isset($_GET['ajouterSms']) && $_GET['ajouterSms'] == "1"){
?>
    <span id="valeur" class ="1"></span>
<?php
    }else{
?>
        
        <span id="valeur" class ="0"></span>
<?php

    }
    
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Luuma: index.php</title>
  <!-- <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" /> -->
  <link href="view/admin/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  
            
        <link href="view/css/styles.css" rel="stylesheet" />
    </head>
    <body id="page-top">

        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="#page-top"><img src="view/ff.png" alt=""></a>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                        <li class="nav-item"><a class="nav-link" href="#services">Services</a></li>
                        <li class="nav-item"><a class="nav-link" href="#about">A propos</a></li>
                        <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <!-- <div class="masthead-subheading">Welcome To Our Studio!</div> -->
                <div class="masthead-heading text-uppercase text-muted font-monospace"><i>Présentation des Services</i></div>
                <a class="btn btn-primary btn-xl text-uppercase text-muted" href="#services">Pour plus information</a>
            </div>
        </header>
        <!-- Services-->
        <section class="page-section" id="services">
        <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Services</h2>
                </div>
                <div class="row">
                    <?php
                        foreach($tabService as $service){
                        // echo $service->getvaleurDate();
                        // die();
                    ?>
                             
                    <div class="col-lg-4 col-sm-6 mb-4">
                        <div class="card h-100 shadow-lg">
                          <img src="" alt="">
                          <div class="card-body">
                                <div class="portfolio-item">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a class="portfolio-link" data-bs-toggle="modal" href="#portfolioModal1">
                                                <div class="portfolio-hover">
                                                    <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                                </div>
                                                <img src="export.php?id=<?=$service->getIdImage()?>" alt="">
                                            
                                            </a>  
                                        </div> 
                                        <div class="col-md-6 ">
                                           <div class="col-md-12 h-100">
                                            <span class="font-monospace">Description</span><br>
                                                
                                                <small class="text-muted"><?=$service->getDescription()?></small>
                                           </div>
                                           <div class = "col-md-12">
                                             <a href="" style="text-decoration: none;" class="alert alert-link"><?=$service->getTypeService()?></a>
                                           </div>
                                        </div>
                                        

                                    </div>
                                    <div class="portfolio-caption">
                                        <div class="portfolio-caption-heading "><?=$service->getLibelle()?></div>
                                        <div class="portfolio-caption-subheading font-monospace"><?=$service->getPrix()?> <i>FCFA</i> </div>
                                
                                    </div>

                            
                                </div>
                            </div>
                            <div class="card-footer">
                            <span class="font-monospace text-muted" >Date Publication: </span> <small class="text-muted"><?=$service->getvaleurDate()?></small>
                            </div>
                        </div>
                       
                    </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </section>

          <!-- About-->
        <section class="page-section" id="about">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">A Propos</h2>
                    <!-- <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3> -->
                </div>
              <div class="row">
              <div class="col-md-10 ">
               <ul class="timeline">
                    <li class="card">
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="dark-footer-bg.jpeg"  alt="..." /></div>
                        <div class="timeline-panel">
                            <div class="col-md-2">
                            <img src="view/admin/exportImage.php?id=<?=$entreprise->getLogo()?>" alt="">

                            </div>
                          
                            <div class="timeline-heading col-md-10">
                                <h4><i class="fa fa-renren" aria-hidden="true"></i><?=$entreprise->getNomEntreprise()?></h4>
                                <blockquote class="subheading"><i class="fa fa-home"></i><?=$entreprise->getAdresse()?></blockquote>
                                <blockquote class="subheading"><i class="fa fa-phone-square"></i> +<?=$entreprise->getTelephone()?></blockquote>
                            </div>
                            <div class="timeline-body"><blockquote ><i class="fa fa-pencil"></i> <span class="text-muted"><?=$entreprise->getDescription()?></span> </blockquote></div>
                        </div>
                    </li>
                   
                </ul>
               </div>
               <div class="col-md-2 ">
                    <div class="">
                      <img src="" alt="">
                      <div class="card-body">
                        <h5 class="card-title">Notes</h5>
                        <br><br><br><br><br><br><br>
                        <div class="text-warning">
                          <i class="fa fa-star" aria-hidden="true"></i>
                          <i class="fa fa-star" aria-hidden="true"></i>
                          <i class="fa fa-star" aria-hidden="true"></i>
                          <i class="fa fa-star" aria-hidden="true"></i>
                          <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                        <a href="#" class="btn btn-primary pull-right"> <i class="fa fa-envelope" aria-hidden="true"></i> </a>
                      </div>
                    </div>
               </div>
             
              </div>
            </div>
        </section>
        
        <!-- Clients-->
        <div class="py-5">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-3 col-sm-6 my-3">
                        <a href="#!"><img class="img-fluid img-brand d-block mx-auto" src="view/assets/img/logos/microsoft.svg" alt="..." /></a>
                    </div>
                    <div class="col-md-3 col-sm-6 my-3">
                        <a href="#!"><img class="img-fluid img-brand d-block mx-auto" src="view/assets/img/logos/google.svg" alt="..." /></a>
                    </div>
                    <div class="col-md-3 col-sm-6 my-3">
                        <a href="#!"><img class="img-fluid img-brand d-block mx-auto" src="view/assets/img/logos/facebook.svg" alt="..." /></a>
                    </div>
                    <div class="col-md-3 col-sm-6 my-3">
                        <a href="#!"><img class="img-fluid img-brand d-block mx-auto" src="view/assets/img/logos/ibm.svg" alt="..." /></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact-->
        <section class="page-section" id="contact">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Contactez - nous</h2>
                    <!-- <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3> -->
                </div>
            
                <!-- to get an API token!-->
                <form id="contactForm" data-sb-form-api-token="API_TOKEN" action="controller/fichierController.php" method="POST" >
                    <div class="row align-items-stretch mb-5">
                        <div class="col-md-6">
                            <div class="form-group">
                                <!-- Name input-->
                                <input class="form-control" id="name" name="nomPrenom" id = "nomPrenom"  required type="text" placeholder="Prénom & Nom *" data-sb-validations="required" />
                                <div class="invalid-feedback" data-sb-feedback="name:required">Un Nom est requis.</div>
                            </div>
                            <div class="form-group">
                                <!-- Email address input-->
                                <input class="form-control" id="email" type="email" required name="email" placeholder="Adresse Email *" data-sb-validations="required,email" />
                                <div class="invalid-feedback" data-sb-feedback="email:required">Un Email est requis.</div>
                                <div class="invalid-feedback" data-sb-feedback="email:email">Email Invalide.</div>
                            </div>
                            <div class="form-group mb-md-0">
                                <!-- Phone number input-->
                                <input class="form-control" id="telephone" type="tel" required  name="telephone"placeholder="Numéro Téléphone *" data-sb-validations="required" />
                                <div class="invalid-feedback" data-sb-feedback="phone:required"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-textarea mb-md-0">
                                <!-- Message input-->
                                <textarea class="form-control" id="message" required name="message" placeholder="Message *" data-sb-validations="required"></textarea>
                                <div class="invalid-feedback" data-sb-feedback="message:required">Un Message est Requis.</div>
                            </div>
                        </div>
                    </div>
                  
                    
                
                    <div class="d-none" id="submitErrorMessage"><div class="text-center text-danger mb-3">Erreur D'envoie du message!</div></div>
                    <!-- Submit Button-->
                    <div class="text-center"><button id="envoyer" class="btn btn-primary btn-xl text-uppercase" name="envoyer" type="submit" id="submitButton" type="submit">Envoyer</button></div>
                </form>
            </div>
        </section>
        <!-- Footer-->
        <footer class="footer py-4">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 text-lg-start"> <img src="view/admin/exportImage.php?id=<?=$entreprise->getLogo()?>" alt=""> Copyright &copy; Luuma</div>
                    <div class="col-lg-4 my-3 my-lg-0">
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fa fa-facebook" aria-hidden="true"></i> </a>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                    <div class="col-lg-4 text-lg-end">
                        <a class="link-dark text-decoration-none me-3" href="#!">ISI Keur massar</a>
                        <a class="link-dark text-decoration-none" href="#!">"Baay  Guinaar"</a>
                    </div>
                </div>
            </div>
        </footer>

       <script>
       
   
       </script>
    </body>
</html>
