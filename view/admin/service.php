  <?php
  session_start();
  require_once '../../bd/Connection.php';
  function chargerClass($class)
  {
    require('../../model/' . $class . '.php');
  }
  spl_autoload_register('chargerClass');

  $typeManager = new TypeServiceManager(getConnection()); 
  $tabTypeService = $typeManager->Lister(); 

  $serviceManager = new ServiceManager(getConnection());
  $tabService = $serviceManager->Lister();

  
  if(isset($_GET['modifierService']) && (int)$_GET['modifierService'] > 0){
    $_SESSION['modifierSer'] = $_GET['modifierService'];
  }else{
    $_SESSION['modifierSer'] = "0";
  }
// var_dump($tabService); die();
  // var_dump($tabAdmin); die();

  ?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <title>Luuma: service.php</title>

    <!-- Bootstrap core CSS -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!--external css-->
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">
    <style>
      body {
        padding-top: 56px;
      }
    </style>
  </head>

  <body>
    <section id="container">
     
      <header class="header black-bg">
        <div class="sidebar-toggle-box">
          <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="#" class="logo"><b>Luuma</b></a>
        <!--logo end-->
        <div class="top-menu">
          <ul class="nav pull-right top-menu">
            <li>
              <a class="logout" href="login.html">Déconnecter</a>
            </li>
          </ul>
        </div>
      </header>
     
      <aside>
        <div id="sidebar" class="nav-collapse ">
          <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">
            <p class="centered">
              <a href="#"><img src="img/ui-sam.jpg" class="img-circle" width="80"></a>
            </p>
            <h5 class="centered"><?= $_SESSION['prenomPersonne'] . " " ?><?= $_SESSION['nomPersonne'] ?></h5>

            <li class="mt">
              <a href="fichierIndex.php">
                <i class="fa fa-dashboard"></i>
                <span>Dashboard</span>
              </a>
            </li>
            <li class="sub-menu">
              <a href="admin.php">
                <i class="fa fa-desktop"></i>
                <span>Administrateur</span>
              </a>
            </li>
            <li class="sub-menu">
              <a href="#">
                <i class="fa fa-desktop"></i>
                <span>Service</span>
              </a>
            </li>
            <li class="sub-menu">
              <a href="typeService.php">
                <i class="fa fa-cogs"></i>
                <span>Type Services</span>
              </a>
          </ul>
          <!-- sidebar menu end-->
        </div>
      </aside>
      
      <section id="main-content" class="mt-12">
        <section class="wrapper">
          <!-- row -->
          <div class="row mt">
            <div class="col-md-12">
              <div class="content-panel">

                <h4 class="mb"><i class="fa fa-angle-right"></i> Ajouter / Modifier <span class="badge badge-pill badge-primary">SERVICE</span></h4>
                <hr>
                <form class="form-horizontal style-form" method="POST" action="../../controller/fichierController.php" enctype="multipart/form-data">

                  <div class="card col-lg-6">
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">Libelle</label>
                      <div class="col-sm-10">
                        <?php
                        if (isset($_GET['modifierService']) && !empty($_GET['modifierService'])) {
                        ?>
                          <input name="libelle" type="text" class="form-control" value="<?= $_SESSION['libelle'] ?>">
                        <?php
                        } else {
                        ?>
                          <input name="libelle" type="text" class="form-control" required>
                        <?php
                        }
                        ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">Catégorie Produit</label>
                      <div class="col-sm-10">
                        <?php
                        if (isset($_GET['modifierService']) && !empty($_GET['modifierService'])) {
                        ?>
                        <div class="col-md-6">
                          <input name="idTypeService" type="text" readonly class="form-control" value="<?= $_SESSION['typeService'] ?>">
                        </div>
                        <div class="col-md-6">
                          <select class="form-control" name="idTypeService" id="">
                              <?php
                              foreach ($tabTypeService as $type) {
                              ?>
                                <option value="<?=$type->getId()?>"><?=$type->getLibelle()?></option>
                              <?php
                              }
                              ?>
                            </select>
                        </div>
                        <?php
                        } else {
                        ?>
                          <select class="form-control" name="idTypeService" id="">
                            <?php
                            foreach ($tabTypeService as $type) {
                            ?>
                              <option value="<?=$type->getId()?>"><?=$type->getLibelle()?></option>
                            <?php
                            }
                            ?>
                          </select>
                        <?php
                        }
                        ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">Image</label>
                      <div class="col-sm-10">
                        <?php
                        if (isset($_GET['modifierService']) && !empty($_GET['modifierService'])) {
                        ?>
                        <div class="col-md-6">
                            <a href="export.php?id=<?=$_SESSION['image']?>&amp;idService=<?=$_GET['modifierService']?>" class="alert alert-link" style="text-decoration: none;">Cliquer - ici</a>
                        </div>
                        <div class="col-md-6">
                            <input id="file" name="image" type="file" class="form-control">
                        </div>
                        <?php
                        } else {
                        ?>
                          <input name="image" type="file" class="form-control" required>
                        <?php
                        }
                        ?>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">Prix</label>
                      <div class="col-sm-10">
                        <?php
                        if (isset($_GET['modifierService']) && !empty($_GET['modifierService'])) {
                        ?>
                          <input name="prix" type="text" class="form-control" value="<?= $_SESSION['prix'] ?>" required>
                        <?php
                        } else {
                        ?>
                          <input name="prix" type="text" class="form-control" required>
                        <?php
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                  <div class="card col-lg-6">
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">Description</label>
                      <div class="col-sm-10">
                        <?php
                        if (isset($_GET['modifierService']) && !empty($_GET['modifierService'])) {
                        ?>
                          <textarea name="description" id="" cols="50" rows="7"><?= $_SESSION['description'] ?></textarea>

                        <?php
                        } else {
                        ?>
                          <textarea name="description" id="" cols="50" rows="7"></textarea>
                        <?php
                        }
                        ?>
                      </div>
                    </div>

                    <div class="text-center">
                      <button id="enregistrerService" name="enregistrerService" class="btn btn-primary btn-xl text-uppercase" id="submitButton" type="submit">Enrégistrer</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="col-md-12">
              <div class="content-panel">
                <table class="table table-striped table-advance table-hover table-bordered table-condensed">
                  <h4><i class="fa fa-angle-right"></i> Liste des <span class="badge badge-pill badge-primary">SERVICES</span></h4>
                  <hr>
                  <thead>
                    <tr>
                      <th><i class="fa fa-bookmark"></i> Id</th>
                      <th><i class="fa fa-bookmark"></i> Libelle</th>
                      <th><i class="fa fa-bullhorn"></i> Prix</th>
                      <th><i class="fa fa-bullhorn"></i> Type Service</th>
                      <th><i class="fa fa-bullhorn"></i> Description</th>
                      <th><i class="fa fa-bullhorn"></i> Prénom</th>
                      <th><i class="fa fa-bullhorn"></i> Nom</th>
                      <th><i class="fa fa-bullhorn"></i> image</th>
                      <th><i class="fa fa-bullhorn"></i> Etat</th>
                      <th><i class="fa fa-bullhorn"></i> Date Enrégistrer</th>
                      <th><i class="fa fa-bullhorn"></i> Date de Modification</th>
                      <th><i class="fa fa-bullhorn"></i> Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach ($tabService as $ad) {
                   
                    ?>
                      <tr>
                        <td><?= $ad->getId() ?></td>
                        <td><?= $ad->getLibelle() ?></td>
                        <td><?= $ad->getPrix() ?></td>
                        <td><?= $ad->getTypeService() ?></td>
                        <td><?= $ad->getDescription() ?></td>
                        <td><?= $ad->getPrenom() ?></td>
                        <td><?= $ad->getNom() ?></td>
                        <td> <img class="" src="exportImage.php?id=<?=$ad->getIdImage()?>" alt=""></td>
                        <td><?= $ad->getEtat() ?></td>
                        <td><?= $ad->getDateEnregistrer() ?></td>
                        <td><?= $ad->getDateModification() ?></td>
                        <td>
                          <a href="../../controller/fichierController.php?idActiverService=<?= $ad->getId() ?>&amp;etatService=<?=$ad->getEtat()?>" class="btn btn-success btn-xs"><i class="fa fa-check"></i></a>
                          <!-- <button type="submit"  class="btn btn-success btn-xs"><i class="fa fa-check"></i></button> -->
                          <a id="idServiceModifier" href="../../controller/fichierController.php?idServiceModifier=<?= $ad->getId() ?>&amp;etatService=<?=$ad->getEtat()?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                          <!-- <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button> -->
                          <a href="../../controller/fichierController.php?idDesactiverService=<?= $ad->getId() ?>&amp;etatService=<?=$ad->getEtat()?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                          <!-- <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button> -->
                        </td>
                      </tr>
                    <?php
                    }
                    ?>
                  </tbody>
                </table>
                <!-- /content-panel -->
              </div>
              <!-- /col-md-12 -->
            </div>
            <!-- /row -->
        </section>
      </section>
      <!-- /MAIN CONTENT -->
      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
        <div class="text-center">
          <p>
            &copy; Copyrights <strong>Luuma</strong>.
          </p>
        </div>
      </footer>
      <!--footer end-->
    </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
    <script src="lib/jquery.scrollTo.min.js"></script>
    <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
    <!--common script for all pages-->
    <script src="lib/common-scripts.js"></script>
    <!--script for this page-->

    <script>
      $(function(){
        $('#file').click(function(){
         $("form").attr("action", "../../controller/fichierController.php?valeur=1");
        
        });
        
   
      })
    </script>

  </body>

  </html>