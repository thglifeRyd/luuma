<?php
    class ServiceManager {
        private $_bd;
        public function __construct($bd)
        {
            $this->setBd($bd);
        }
        public function setBd($bd){
            $this->_bd = $bd;
        }
        // ---------------------------------------


        public function selectImage(){
            $requet = $this->_bd->query("SELECT * FROM image");
            $tabImage = array();
            while($donne = $requet->fetch(PDO::FETCH_ASSOC)){
                $tabImage[] = new Image($donne);
            }
            return $tabImage;

        }
        public function Ajouter(Service $service)
        {
        //  var_dump($service->getIdTypeService()); die();
        $requet = $this->_bd->prepare("INSERT INTO Service(libelle, description, prix, image, idAdministrateur, idTypeService, dateEnregistrer) VALUE(?, ?, ?, ?, ?, ?, NOW())");
        try{
            $requet->execute(array(
                $service->getLibelle(),
                $service->getDescription(),
                $service->getPrix(),
                  $service->getImage(),
                  $service->getIdAdministrateur(),
                  $service->getIdTypeService()
                 
             ));
        }catch(Exception $ex){
            var_dump($ex->getMessage());
        }
           
           return true;
        }   
        public function Modifier(Service $service)
        {
        //    var_dump($service->getId()); die();
            try {

            $requet = $this->_bd->prepare('UPDATE Service SET libelle = :lib, description = :desc, prix = :prix, 
            idTypeService = :idType, idAdministrateur = :admin, dateModification = now() WHERE id = :id
            ');
            $requet->bindValue(':lib', $service->getLibelle());
            $requet->bindValue(':desc', $service->getDescription());
            $requet->bindValue(':prix', $service->getPrix());
            $requet->bindValue(':idType', $service->getIdTypeService());
            $requet->bindValue(':id', $service->getId(), PDO::PARAM_INT);
            $requet->bindValue(':admin', $service->getIdAdministrateur(), PDO::PARAM_INT);
            $requet->execute();
            }catch(Exception $ex){
                var_dump($ex->getMessage());
            }

            return true;
        }
        public function Lister()
        {
            $requet = $this->_bd->query("SELECT Service.id, Service.libelle, Service.description, Service.prix, Service.etat, Service.dateEnregistrer, Service.dateModification,
            image.id AS idImage, image.nom as nomImage, image.taille as tailleImage, image.type as typeImage, image.bin as binImage, 
            Administrateur.nom, Administrateur.prenom, Administrateur.email,
            TypeService.libelle AS typeService  FROM Service 
            INNER JOIN image
            ON Service.image = image.id 
            INNER JOIN Administrateur
            ON Service.idAdministrateur = Administrateur.id
            INNER JOIN TypeService
            ON TypeService.id = Service.idTypeService");

                // var_dump($requet->fetchAll(2)); die();
            $services = array();
            while($donne = $requet->fetch(PDO::FETCH_ASSOC)){
                $services[] = new Service($donne);
            }
            return $services;
        }
      
        public function SelectServices()
        {
            $requet = $this->_bd->query("SELECT Service.id, Service.libelle, Service.description, Service.prix, Service.etat, Service.dateEnregistrer, Service.dateModification,
            image.id AS idImage, image.nom as nomImage, image.taille as tailleImage, image.type as typeImage, image.bin as binImage, 
            Administrateur.nom, Administrateur.prenom, Administrateur.email,
            TypeService.libelle AS typeService  FROM Service 
            INNER JOIN image
            ON Service.image = image.id 
            INNER JOIN Administrateur
            ON Service.idAdministrateur = Administrateur.id
            INNER JOIN TypeService
            ON TypeService.id = Service.idTypeService WHERE Service.etat =1");

                // var_dump($requet->fetchAll(2)); die();
            $services = array();
            while($donne = $requet->fetch(PDO::FETCH_ASSOC)){
                $services[] = new Service($donne);
            }
            return $services;
        }
        public function getListeService(){
            $requet = $this->_bd->query("SELECT 
                S.id, S.libelle, S.description, S.prix, S.dateEnregister, S.dateModification, S.etat
                I.id, I.bin, 
                T.libelle, A.nom, A.prenom, A.email 
                FROM Service AS S INNER JOIN image As I
                ON S.image = I.id
                INNER JOIN TypeService AS T
                ON S.idTypeService = T.id
                INNER JOIN Administrateur
                ON S.idAdministrateur = Administrateur.id
                ");
              $services = array();
              while($donne = $requet->fetch(PDO::FETCH_ASSOC)){
                  $services[] = new Service($donne);
              }
              return $services;   
        }
        public function affichierImage($id){
            $requet = $this->_bd->prepare("SELECT * from image where id = ? limit 1");
            $requet->setFetchMode(PDO::FETCH_ASSOC);
            $requet->execute(array($_GET['id']));
            $tab = $requet->fetchAll();
            return $tab[0]["bin"];
        }
        // ___________________________________________
        public function Activer(int $id)
        {
            $requet = $this->_bd->prepare("UPDATE Service SET etat = :etat WHERE id = :id");
            $requet->bindValue(':etat', 1, PDO::PARAM_INT);
            $requet->bindValue(':id', $id, PDO::PARAM_INT);
            $requet->execute();
            return true;
        }
        public function Desactiver(int $id)
        {
     
            $requet = $this->_bd->prepare("UPDATE Service SET etat = :etat WHERE id = :id");
            $requet->bindValue(':etat', 0, PDO::PARAM_INT);
            $requet->bindValue(':id', $id, PDO::PARAM_INT);
            $requet->execute();
            return true;
        }
        public function SelectService($id){
            
            $requet = $this->_bd->prepare('SELECT Service.libelle, Service.description, Service.prix, Service.image,
                TypeService.libelle as typeService from Service
                inner JOIN TypeService
                on Service.idTypeService = TypeService.id
                WHERE Service.id = :id AND Service.etat = 1');
            $requet->bindValue(':id', $id, PDO::PARAM_INT);
            $requet->execute();
          
            return new Service($requet->fetch(PDO::FETCH_ASSOC));
            
        }

       
    }
?>