<?php

    class Image{
        private $_id;
        private $_bin;
        private $_nom;
        private $_taille;
        private $_type;

        public function __construct($donne)
        {
            $this->hydratation($donne);
        }
        public function setId($id){
            $this->_id = $id;
        }
        public function setBin($bin){
            $this->_bin = $bin;
        }
        public function setNom($nom){
            $this->_nom = $nom;
        }
        public function setTaille($taille){
            $this->_taille = $taille;
        }
        public function setType($type){
            $this->_type = $type;
        }
        public function getId(){
            return $this->_id;
        }
        public function getBin(){
            return $this->_bin;
        }
        public function getNom(){
            return $this->_nom;
        }
        public function getTaille(){
            return $this->_taille;
        }
        public function getType(){
            return $this->_type;
        }
        function hydratation(array $donne)
        {
           foreach($donne as $key => $value){
               $method = 'set'. ucfirst($key);
               if(method_exists($this, $method)){
                   $this->$method($value);
               }
           }
        }
    }
?>