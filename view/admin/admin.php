<?php
session_start();
require_once '../../bd/Connection.php';
function chargerClass($class){
  require ('../../model/'. $class. '.php');
}    
spl_autoload_register('chargerClass');
  $bd = getConnection();
 
  $adminManager = new AdministrateurManager($bd);
    $tabAdmin = $adminManager->Lister();
      
    // $adminManager = new AdministrateurManager(getConnection());
    $tabContact = $adminManager->ListerContact();
    // var_dump($tabContact); die();
    // var_dump($tabAdmin); die();
 if(isset($_GET['modifier']) && !empty($_GET['modifier'])){
   $_SESSION['modifier'] = $_SESSION['id'];
 }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Luuma: admin.php</title>

  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  <style>
      body {
         padding-top: 56px;
      }
    </style>
</head>

<body>
  <section id="container" class = "mt-12">
  
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="#" class="logo"><b>Luuma</b></a>
      <!--logo end-->
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li>
            <a class="logout" href="login.html">Deconnecter</a>
          </li>
        </ul>
      </div>
    </header>
    
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered">
            <a href="#"><img src="img/ui-sam.jpg" class="img-circle" width="80"></a>
          </p>
          <h5 class="centered"><?=$_SESSION['prenomPersonne']. " "?><?=$_SESSION['nomPersonne']?></h5>

          <li class="mt">
            <a href="fichierIndex.php">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
              <a href="#">
                <i class="fa fa-desktop"></i>
                <span>Administrateur</span>
              </a>
            </li>
          <li class="sub-menu">
            <a href="service.php">
              <i class="fa fa-desktop"></i>
              <span>Service</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="typeService.php">
              <i class="fa fa-cogs"></i>
              <span>Type Services</span>
              </a>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
   
    <section id="main-content" class="mt-10">  
      <section class="wrapper">
        <!-- row -->
        <div class="row mt">
        <div class="col-md-12">
            <div class="content-panel">
           
              <h4 class="mb"><i class="fa fa-angle-right"></i> Ajouter / Modifier Administrateur</h4>
              <hr>
              <form class="form-horizontal style-form" method="POST" action="../../controller/fichierController.php">
             
                 <div class="card col-lg-6">
                 <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Prenom</label>
                  <div class="col-sm-10">
                    <?php
                     if(isset($_GET['modifier']) && !empty($_GET['modifier'])){
                    ?>
                    <input name="prenom" type="text" class="form-control" value="<?=$_SESSION['prenom']?>" required>
                    <?php
                     }else{
                    ?>
                    <input name="prenom" type="text" class="form-control" required>
                    <?php
                     }
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Nom</label>
                  <div class="col-sm-10">
                    <?php
                      if(isset($_GET['modifier']) && !empty($_GET['modifier']))
                      {
                    ?>
                    <input name="nom" type="text" class="form-control" value="<?=$_SESSION['nom']?>"  required>
                    <?php
                      }else{
                    ?>
                    <input name="nom" type="text" class="form-control"  required>
                    <?php
                      }
                    ?>
                    
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Telephone</label>
                  <div class="col-sm-10">
                    <?php
                      if(isset($_GET['modifier']) && !empty($_GET['modifier'])){
                    ?>
                      <input name="telephone" type="text" class="form-control" value="<?=$_SESSION['telephone']?>" required>
                    <?php
                      }else{
                    ?>
                      <input name="telephone" type="text" class="form-control" required>
                    <?php
                      }
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                  <?php
                     if(isset($_GET['modifier']) && !empty($_GET['modifier'])){
                    ?>
                    <input name="email" type="email" class="form-control" value="<?=$_SESSION['email']?>"  required>
                  <?php
                     }else{
                    ?>
                    <input name="email" type="email" class="form-control" required>
                  <?php
                     }
                    ?>
                  </div>
                </div>
               
               </div>
               <div class="card col-lg-6">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Login</label>
                    <div class="col-sm-10">
                    <?php
                     if(isset($_GET['modifier']) && !empty($_GET['modifier'])){
                    ?>
                      <input name="login" type="text" class="form-control" value="<?=$_SESSION['login']?>"  required>
                    <?php
                     }else{
                    ?>
                      <input name="login" type="text" class="form-control" required>
                    <?php
                     }
                    ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Mot de Passe</label>
                    <div class="col-sm-10">
                      <?php
                      if(isset($_GET['modifier']) && !empty($_GET['modifier'])){
                      ?>
                        <input name="motPasse" type="password" class="form-control" value="<?=$_SESSION['motPasse']?>"  required>
                      <?php
                      }else{
                      ?>
                        <input name="motPasse" type="password" class="form-control" required>
                      <?php
                      }
                      ?>
                    </div>
                  </div>
                  <div class="col-md-10">
                    <div class="col-md-4 text-center">
                        <button name="enregistrerAdmin" class="btn btn-primary btn-xl text-uppercase" id="submitButton" type="submit">Enrégistrer</button>
                    </div>
                  </div>
              </div>
              </form>
            </div>
        </div>
          <div class="col-md-12 mt">
            <div class="content-panel">
              <table class="table table-striped table-advance table-hover table-bordered table-condensed">
                <h4><i class="fa fa-angle-right"></i> Liste Administrateur</h4>
                <hr>
                <thead>
                  <tr>
                    <th><i class="fa fa-bookmark"></i> Id</th>
                    <th><i class="fa fa-bookmark"></i> Prenom</th>
                    <th><i class="fa fa-bullhorn"></i> Nom</th>
                    <th><i class="fa fa-bullhorn"></i> Telephone</th>
                    <th><i class="fa fa-bullhorn"></i> Email</th>
                    <th><i class="fa fa-bullhorn"></i> Login</th>
                    <th><i class="fa fa-bullhorn"></i> Mot de Passe</th>
                    <th><i class="fa fa-bullhorn"></i> Etat</th>
                    <th><i class="fa fa-bullhorn"></i> Date Enregistrer</th>
                    <th><i class="fa fa-bullhorn"></i> Date de Modification</th>  
                    <th><i class="fa fa-bullhorn"></i> Actions</th>  
                  </tr>
                </thead>
                <tbody>
                <?php
                    foreach($tabAdmin as $ad){
                      // var_dump($ad);
                      ?>
                      <tr>
                        <td><?=$ad->getId()?></td>
                        <td><?=$ad->getPrenom()?></td>
                        <td><?=$ad->getNom()?></td>
                        <td><?=$ad->getTelephone()?></td>
                        <td><?=$ad->getEmail()?></td>
                        <td><?=$ad->getLogin()?></td>
                        <td><?=$ad->getMotPasse()?></td>
                        <td><?=$ad->getEtat()?></td>
                        <td><?=$ad->getDateEnregistrer()?></td>
                        <td><?=$ad->getDateModification()?></td>
                        <td>
                          <a href="../../controller/fichierController.php?idActiver=<?=$ad->getId()?>" class="btn btn-success btn-xs"><i class="fa fa-check"></i></a>
                        <!-- <button type="submit"  class="btn btn-success btn-xs"><i class="fa fa-check"></i></button> -->
                          <a href="../../controller/fichierController.php?id=<?=$ad->getId()?>&amp;etat=<?=$ad->getEtat()?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                        <!-- <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button> -->
                          <a href="../../controller/fichierController.php?idDesactiver=<?=$ad->getId()?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                        <!-- <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button> -->
                        </td>
                      </tr>
                  <?php
                    }
                  ?>
                </tbody>
              </table>
            <!-- /content-panel -->
          </div>
          <!-- /col-md-12 -->
        </div>



        <div class="col-md-12 mt">
            <div class="content-panel">
              <table class="table table-striped table-advance table-hover table-bordered table-condensed">
                <h4><i class="fa fa-angle-right"></i> Contact</h4>
                <hr>
                <thead>
                  <tr>
                    <th><i class="fa fa-bookmark"></i> Id</th>
                    <th><i class="fa fa-bookmark"></i> Nom Prenom</th>
                    <th><i class="fa fa-bullhorn"></i> Telephone</th>
                    <th><i class="fa fa-bullhorn"></i> Email</th>
                    <th><i class="fa fa-bullhorn"></i> Message</th>
                    <th><i class="fa fa-bullhorn"></i> Date Enregistrer</th>
                    <th><i class="fa fa-bullhorn"></i> Actions</th>  
                  </tr>
                </thead>
                <tbody>
                <?php
                    foreach($tabContact as $ad){
                      // var_dump($ad);
                      ?>
                      <tr>
                        <td><?=$ad->getId()?></td>
                        <td><?=$ad->getNomPrenom()?></td>
                        <td><?=$ad->getTelephone()?></td>
                        <td><?=$ad->getEmail()?></td>
                        <td><?=$ad->getMessage()?></td>
                        <td><?=$ad->getDateEnregistrer()?></td>
                        <td>
                          <a href="../../controller/fichierController.php?idDesactiverContact=<?=$ad->getId()?>" class="btn btn-danger btn-md"><i class="fa fa-trash-o ">  supprimer</i></a>
                        <!-- <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button> -->
                        </td>
                      </tr>
                  <?php
                    }
                  ?>
                </tbody>
              </table>
            <!-- /content-panel -->
          </div>
          <!-- /col-md-12 -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Luuma</strong>. 
        </p>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  
</body>

</html>
