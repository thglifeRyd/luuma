<?php
    interface IEntreprise{
        public function creerEntreprise(Entreprise $entreprise);
        public function AfficherEntreprise();
        public function ModifierEntreprise(Entreprise $entreprise);
        public function AfficherAdministrateur(int $id);
    }
?>