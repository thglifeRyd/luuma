<?php
   require_once "bd/Connection.php";
   $bd = getConnection();

   $req = $bd->prepare("SELECT * FROM image WHERE id = ?");
   $req->execute(array($_GET["id"]));
   $req->setFetchMode(PDO::FETCH_ASSOC);
   $tab = $req->fetchAll();
   echo $tab[0]["bin"];
?>