<?php
    class Service {
        private $_id;
        private $_libelle;
        private $_prix;
        private $_description;
        private $_idTypeService;
        private $_etat;
        private $_idAdministrateur;
        private $_dateEnregistrer;
        private $_dateModification;
        private $_image;
        private $_nom;
        private $_prenom;
        private $_bin;
        private $_idImage;
        private $_typeService;
        private $_email;

        public function __construct(array $donne)
        {
            # code...
            $this->hydratation($donne);
        }
        public function getId(){
            return $this->_id;
        }
        public function getLibelle(){
            return $this->_libelle;
        }
        public function getPrix(){
            return $this->_prix;
        }
        public function getDescription(){
            return $this->_description;
        }
        public function getIdTypeService(){
            return $this->_idTypeService;
        }
        public function setId($id){
            
                $this->_id = $id;
            
        }
        public function setPrix($prix){
            $this->_prix = $prix;
        }
        public function setLibelle($lib){
            $this->_libelle = $lib;
        }
        public function setDescription($desc){
            $this->_description = $desc;
        }
        public function setIdTypeService($id){
            $this->_idTypeService = $id;
        }

        public function getEtat(){
            return $this->_etat;
        }
        public function setEtat($etat){
           
                $this->_etat = $etat;
            
        }
        public function getIdAdministrateur(){
            return $this->_idAdministrateur;
        }
        public function setIdAdministrateur($id){
            $this->_idAdministrateur = $id;
        }
        public function setDateEnregistrer($date){
            $this->_dateEnregistrer = $date;
        }
        public function setDateModification($date){
            $this->_dateModification = $date;
        }
        public function getDateEnregistrer(){
            return $this->_dateEnregistrer;
        }
        public function getDateModification(){
            return $this->_dateModification;
        }
        public function getImage(){
            return $this->_image;
        }
        public function setImage($img){
            $this->_image = $img;
        }
        public function setNom($nom){
            $this->_nom = $nom;
        }
        public function setPrenom($prenom){
            $this->_prenom = $prenom;
        }
        public function setIdImage($id){
            $this->_idImage = $id;
        }
        public function setBin($bin){
            $this->_bin = $bin;
        }
        public function getNom(){
            return $this->_nom;
        }
        public function getPrenom(){
            return $this->_prenom;
        }
        public function getIdImage(){
            return  $this->_idImage;
        }
        public function getBin(){
            return $this->_bin;
        }
        public function setTypeService($type){
            $this->_typeService = $type;
        }
        public function setEmail($email){
            $this->_email = $email;
        }
        public function getTypeService(){
            return $this->_typeService;
        }
        public function getEmail(){
            return $this->_email;
        }
        public function hydratation(array $donne)
        {
           foreach($donne as $key => $value){
               $method = 'set'. ucfirst($key);
               if(method_exists($this, $method)){
                   $this->$method($value);
               }
           }
        }   
        public function getvaleurDate(){
            return strftime('%d/%m/%Y à %H:%M:%S', strtotime($this->_dateEnregistrer));
        }   

    }
?>