<?php
    class EntrepriseManager {
        private $_bd;
        public function __construct($bd)
        {
            $this->setBd($bd);
        }
        public function setBd($bd)
        {
            # code...
            $this->_bd = $bd;
        }


          //--------------------------------------------------------
          public function CreerEntreprise(Entreprise $entreprise)
          {
            $requet = $this->_bd->prepare('INSERT Entreprise set nomEntreprise = :nom, description = :desc,
                adresse = :add, telephone = :tel, idAdministrateur = :idAdmin, logo = :logo, dateEnregistrer = Now() 
            ');
            try{

            $requet->execute(array(
                ':nom' => $entreprise->getNomEntreprise(),
                ':desc' => $entreprise->getDescription(),
                ':add' => $entreprise->getAdresse(),
                ':tel' => $entreprise->getTelephone(),
                ':idAdmin' => $entreprise->getIdAdministrateur(),
                ':logo' => $entreprise->getLogo()
            ));   
            }catch(Exception $ex){
                var_dump($ex->getMessage()); die();
            }

            return true;
          }
          public function ModifierEntreprise(Entreprise $entreprise)
          {
            $requet = $this->_bd->exec('UPDATE Entreprise SET nomEntreprise = :nom, description = :desc,
            telephone = :tel, adresse = :add, idAdministrateur = :idAdmin');
            
            $requet->execute(array(
                ':nom' => $entreprise->getNomEntreprise(),
                ':desc' => $entreprise->getDescription(),
                ':tel' => $entreprise->getTelephone(),
                ':add' => $entreprise->getAdresse(),
                ':idAdmin' => $entreprise->getIdAdministrateur()
            ));
            return true;
          }
          public function AfficherEntreprise()
          {
              $requet = $this->_bd->query("SELECT * FROM Entreprise");
              return new Entreprise($requet->fetch(PDO::FETCH_ASSOC));
          }
          public function AfficherAdministrateur(int $idAdmin){
             $requet = $this->_bd->prepare("SELECT nom, prenom, telephone, email from Administrateur 
              INNER JOIN Entreprise on
              Administrateur.id = Entreprise.idAdministrteur 
              WHERE Entreprise.idAdministrateur = :id");
            $requet->bindValue(':id', $idAdmin, PDO::PARAM_INT);
            $requet->execute();
              return null;
          }

    }
?>