<?php
session_start();
require_once '../../bd/Connection.php';
function chargerClass($class){
  require ('../../model/'. $class. '.php');
}    
spl_autoload_register('chargerClass');
 
  $entrepriseManager = new EntrepriseManager(getConnection());
    $entreprise = $entrepriseManager->AfficherEntreprise();
    // var_dump($entreprise->getLogo()); die();

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/zabuto_calendar.css">
  <link rel="stylesheet" type="text/css" href="lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  <title>luuma:index.html</title>
  <script src="lib/chart-master/Chart.js"></script>

</head>

<body>
  <section id="containe" >
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="index.html" class="logo"><b>LUUMA</b></a>
      <!--logo end-->
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="login.php">Se Déconnecter</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="#"><img src="img/ui-sam.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered"><?=$_SESSION['prenomPersonne']. " "?><?=$_SESSION['nomPersonne']?></h5>
          <li class="mt">
            <a class="active" href="#">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="admin.php">
              <i class="fa fa-desktop"></i>
              <span>Administrateur</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="service.php">
              <i class="fa fa-cogs"></i>
              <span>Service</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="typeService.php">
              <i class="fa fa-book"></i>
              <span>Type Service</span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content" class="mt-12">
        <div class="col-md-10 mt alert">
          <div class="col-lg-6 col-md-6 col-sm-6 alert mt">
            <h4 class="title"> <span class="badge badge-pill badge-primary">Présentation Entreprise</span> </h4>
            <div id="message"></div>
            <form class="contact-form php-mail-form"  enctype="multipart/form-data"role="form" action="../../controller/fichierController.php" method="POST">

              <div class="form-group">
                <?php
                  if(isset($_GET['chargerEntreprise'])){
                ?>
                <input type="text" value="<?=$_SESSION['nomEntreprise']?>" name="nomEntreprise" class="form-control" id="contact-name" placeholder="Nom Entreprise" data-rule="minlen:4" data-msg="Please enter at least 4 chars" >
                <?php
                  }else{
                ?>
                <input type="text" required name="nomEntreprise" class="form-control" id="contact-name" placeholder="Nom Entreprise" data-rule="minlen:4" data-msg="Please enter at least 4 chars" >

                <?php
                  }
                ?>
                <div class="validate"></div>
              </div>
              <div class="form-group">
                 <?php
                  if(isset($_GET['chargerEntreprise'])){
                 ?>

                <input type="text" value="<?=$_SESSION['adresse']?>" name="adresse" class="form-control" id="contact-email" placeholder="Adresse Entreprise" data-rule="email" data-msg="Please enter a valid email">
                <?php
                  }else{
                 ?>
                <input type="text" required name="adresse" class="form-control" id="contact-email" placeholder="Adresse Entreprise" data-rule="email" data-msg="Please enter a valid email">

                <?php

                  }
                 ?>
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <?php
                  if(isset($_GET['chargerEntreprise'])){
                ?>

                <input type="text" value="<?=$_SESSION['telephone']?>" name="telephone" class="form-control" id="contact-subject" placeholder="Téléphone Entreprise" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
                <?php
                  }else{
                ?>
                <input type="text" required name="telephone" class="form-control" id="contact-subject" placeholder="Téléphone Entreprise" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">

                <?php

                  }
                ?>
                <div class="validate"></div>
              </div>

              <div class="form-group">
                 <?php
                  if(isset($_GET['chargerEntreprise'])){
                 ?>
                     <input id="file" name="logo" type="file" class="form-control">
                <?php
                  }else{
                 ?>
                     <input name="logo" type="file" class="form-control">

                <?php
                  }
                 ?>
                <div class="validate"></div>
              </div>

              <div class="form-group">
                <?php
                  if(isset($_GET['chargerEntreprise'])){
                ?>

                <textarea class="form-control"  name="description" id="contact-message" placeholder="Description Entreprise" rows="5" data-rule="required" data-msg="Please write something for us"><?=$_SESSION['descriptionEntreprise']?></textarea>
                <?php

                  }else{
                ?>

                <textarea class="form-control" required name="description" id="contact-message" placeholder="Description Entreprise" rows="5"  data-rule="required" data-msg="Please write something for us"></textarea>
                <?php

                  }
                ?>
                <div class="validate"></div>
              </div>

              <div class="form-send">
                <?php
                  if(isset($_GET['chargerEntreprise'])){
                ?>

                    <button name="ModifierEntreprise" type="submit" class="btn btn-large btn-primary">Enregistrer</button>
                <?php
                  }else{
                ?>

                    <button name="enregistrerEntreprise" type="submit" class="btn btn-large btn-primary">Enregistrer</button>
                    <?php

                  }
                ?>
              </div>

            </form>
          </div>

          <div class="col-lg-6 col-md-6 col-sm-6 alert mt">
            <div class="row">
            <h4 class="title"> <span class="badge badge-pill badge-primary">Détail Entreprise</span> </h4>
            <blockquote><?=$entreprise->getNomEntreprise()?></blockquote>
            <blockquote>
               <p><?=$entreprise->getDescription()?></p>
                  <ul class="">
                    <li><i class="fa fa-phone-square"></i> +<?=$entreprise->getTelephone()?></li>
                    <li><i class="fa fa-home"></i> <?=$entreprise->getAdresse()?></li>
                  </ul>
            </blockquote>

                 <div class="col-md-6 ">
                      <img src="exportImage.php?id=<?=$entreprise->getLogo()?>" alt="">  
                 </div> 
                 <div class="col-md-3">
                  <a href="../../controller/fichierController.php?entreprise=<?=$entreprise->getNomEntreprise()?>" class="alert alert-link btn btn-default">modifier</a>

                 </div>                
            </div>
            
            
            </div>
          
            <!-- contact_details -->
          </div>
        </div>  
                
       
    </section>
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Luuma</strong>.
        </p>
        <div class="credits">
          <!--
            You are NOT allowed to delete the credit link to TemplateMag with free version.
            You can delete the credit link only if you bought the pro version.
            Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/dashio-bootstrap-admin-template/
            Licensing information: https://templatemag.com/license/
          -->
        
        </div>
        <a href="index.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>

  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <script type="text/javascript" src="lib/gritter/js/jquery.gritter.js"></script>
  <script type="text/javascript" src="lib/gritter-conf.js"></script>
  <!--script for this page-->
  <script src="lib/sparkline-chart.js"></script>
  <script src="lib/zabuto_calendar.js"></script>
 
  <script type="application/javascript">

      $(function(){
        $('#file').click(function(){
         $("form").attr("action", "../../controller/fichierController.php?valeur=1");
        
        });
        
   
      });
    $(document).ready(function() {
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        },
        legend: [{
            type: "text",
            label: "Special event",
            badge: "00"
          },
          {
            type: "block",
            label: "Regular event",
          }
        ]
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }




  </script>
</body>

</html>
