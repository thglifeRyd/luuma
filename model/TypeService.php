<?php
    class TypeService{
        private $_id;
        private $_libelle;
        private $_descritption;
        private $_dateEnregistrer;
        private $_dateModification;
        private $_etat;
        
        public function __construct(array $donne)
        {
            $this->hydratation($donne);
        }
        public function setId($id){
         
                $this->_id = $id;
        }
        public function setLibelle($lib){
            $this->_libelle = $lib;
        }
        public function getId(){
            return $this->_id;
        }
        public function getLibelle(){
            return $this->_libelle;
        }
        public function setDateEnregistrer($date){
            $this->_dateEnregistrer = $date;
        }
        public function setDateModification($date){
            $this->_dateModification = $date;
        }
        public function getDateEnregistrer(){
            return $this->_dateEnregistrer;
        }
        public function getDateModification(){
            return $this->_dateModification;
        }
        public function getEtat(){
            return $this->_etat;
        }
        public function setEtat($etat){
            $this->_etat = $etat;
        }
        public function setDescription($desc){
            $this->_descritption = $desc;
        }
        public function getDescription(){
            return $this->_descritption;
        }
        public function hydratation(array $donne)
        {
           foreach($donne as $key => $value){
               $method = 'set'. ucfirst($key);
               if(method_exists($this, $method)){
                   $this->$method($value);
               }
           }
        }
    }
?>