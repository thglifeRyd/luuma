<?php
session_start();
require_once "../bd/Connection.php";
function chargerClass($class)
{
  require('../model/' . $class . '.php');
}
spl_autoload_register('chargerClass');
$bd = getConnection();
//------------------------------------------------------------------------


if(isset($_GET['idDesactiverService']) && !empty($_GET['idDesactiverService']) && isset($_GET['etatService'])){
  if($_GET['etatService'] == "0"){
    header('location:../view/admin/service.php');
  }else{
    $serviceManager = new ServiceManager(getConnection());
    
    if($serviceManager->Desactiver($_GET['idDesactiverService'])){
      header('location:../view/admin/service.php');
    }
  }
 
}

if(isset($_GET['idActiverService']) && !empty($_GET['idActiverService']) && isset($_GET['etatService'])){
  if($_GET['etatService'] == "1"){
    header('location:../view/admin/service.php');
  }else{
    $serviceManager = new ServiceManager(getConnection());
    
    if($serviceManager->Activer($_GET['idActiverService'])){
      header('location:../view/admin/service.php');
    }
  }
 
} 


if(isset($_GET['retourService']) && !empty($_GET['retourService'])){
  // var_dump("valeur"); die(); 
  $serviceManager = new ServiceManager(getConnection());
  $service = $serviceManager->SelectService($_GET['retourService']);

  if ($service) {
    $_SESSION['libelle'] = $service->getLibelle();
    $_SESSION['description'] = $service->getDescription();
    $_SESSION['prix'] = $service->getPrix();
    $_SESSION['typeService'] = $service->getTypeService();
    $_SESSION['image'] = $service->getImage();


    header('location:../view/admin/service.php?modifierService='. $_GET['retourService']);
  }
}

if(isset($_GET['idServiceModifier']) && !empty($_GET['idServiceModifier'])){
  
  if($_GET['etatService'] == "0"){
    header('location:../view/admin/service.php');
  }else{
  
    $serviceManager = new ServiceManager(getConnection());
    $service = $serviceManager->SelectService($_GET['idServiceModifier']);

    if ($service) {
      $_SESSION['libelle'] = $service->getLibelle();
      $_SESSION['description'] = $service->getDescription();
      $_SESSION['prix'] = $service->getPrix();
      $_SESSION['typeService'] = $service->getTypeService();
      $_SESSION['image'] = $service->getImage();
  
      header('location:../view/admin/service.php?modifierService='. $_GET['idServiceModifier']);
    }
  }
}

 if(isset($_SESSION['modifierSer']) && (int)$_SESSION['modifierSer'] > 0 && isset($_POST['enregistrerService'])){

  
  
  if(isset($_POST['libelle']) && isset($_POST['idTypeService']) && isset($_POST['prix']) && isset($_POST['description'])){
    $bd = getConnection(); //  
    if(isset($_GET['valeur']) && $_GET['valeur'] == "1"){
        try{
          $requete = $bd->prepare('UPDATE image SET nom = ?, taille = ?, type = ?, bin = ? WHERE id = ?');
          $requete->execute(array(
            $_FILES['image']['name'],
            $_FILES['image']['size'],
            $_FILES['image']['type'],
            file_get_contents($_FILES['image']['tmp_name']),
            $_SESSION['image']
          ));
          $requete->closeCursor();
        }catch(Exception $ex){
          var_dump($ex->getMessage()); die();
        }
      }
      // var_dump("modifier"); die();
       
    $manager = new ServiceManager($bd);
    $bool = $manager->Modifier(new Service(array(
      'libelle' => $_POST['libelle'],
      'prix' => $_POST['prix'],
      'description' => $_POST['description'],
      'idTypeService' => $_POST['idTypeService'],
      'id' => $_SESSION['modifierSer'],
      'idAdministrateur' => $_SESSION['idPersonne']
    )));
        if($bool){
          $_SESSION['modifierSer'] =  0;
           header('location:../view/admin/service.php');
        }
    }
 }
  
    
  


if (isset($_POST['connecter']) && isset($_POST['login']) && isset($_POST['motPasse'])) {
  
  if (!empty($_POST['login']) && !empty($_POST['motPasse'])) {
    $admin = new AdministrateurManager(getConnection());
    $login = strtolower($_POST['login']);
    $motPasse = $_POST['motPasse'];
    $id = $admin->Exists($login, $motPasse);
    if ($id) {
      $_SESSION['idPersonne'] = $id['id'];
      $_SESSION['nomPersonne'] = $id['nom'];
      $_SESSION['prenomPersonne'] = $id['prenom'];
      header('location:../view/admin/fichierIndex.php');
    } else {
      header('location:../view/admin/index.php');
    }
  }
}
if (isset($_SESSION['modifier'])) {
  if (isset($_POST['enregistrerAdmin'])) {
    if (
      !empty($_POST['nom']) && !empty($_POST['prenom'])
      && !empty($_POST['telephone']) && !empty($_POST['email'])
      && !empty($_POST['login']) && !empty($_POST['motPasse'])
    ) {
      $admin = new AdministrateurManager(getConnection());
      // var_dump("passe"); die();
      $bool = $admin->Modifier(new Administrateur($_POST), $_SESSION['modifier']);
      if ($bool) {
        header('location:../view/admin/admin.php');
      }
    }
  }
} else {
  if (isset($_POST['enregistrerAdmin'])) {
    if (
      !empty($_POST['nom']) && !empty($_POST['prenom'])
      && !empty($_POST['telephone']) && !empty($_POST['email'])
      && !empty($_POST['login']) && !empty($_POST['motPasse'])
    ) {
      $admin = new AdministrateurManager(getConnection());
      if ($admin->Ajouter(new Administrateur($_POST))) {
        header('location:../view/admin/admin.php?ajouter=1');
      }
    }
  }
}

if (isset($_GET['id']) && !empty($_GET['id'])) {
  if ($_GET['etat'] == "0") {
    header('location:../view/admin/admin.php');
  } else {
    $admin = new AdministrateurManager(getConnection());

    $administrateur = $admin->SelectAdmin($_GET['id']);
    // var_dump($administrateur); die();
    if ($administrateur) {
      $_SESSION['nom'] = $administrateur->getNom();
      $_SESSION['prenom'] = $administrateur->getPrenom();
      $_SESSION['telephone'] = $administrateur->getTelephone();
      $_SESSION['email'] = $administrateur->getEmail();
      $_SESSION['motPasse'] = $administrateur->getMotPasse();
      $_SESSION['login'] = $administrateur->getLogin();
      $_SESSION['id'] = $administrateur->getId();

      header('location:../view/admin/admin.php?modifier=1');
    }
  }
}
if (isset($_GET['idDesactiver']) && !empty($_GET['idDesactiver'])) {
  $admin = new AdministrateurManager(getConnection());
  $bool = $admin->Desactiver($_GET['idDesactiver']);
  if ($bool) {
    header('location:../view/admin/admin.php');
  }
}
if (isset($_GET['idActiver']) && !empty($_GET['idActiver'])) {
  $admin = new AdministrateurManager(getConnection());
  $bool = $admin->Activer($_GET['idActiver']);
  if ($bool) {
    header('location:../view/admin/admin.php');
  }
}


if (isset($_POST['ModifierTypeService'])) {
  // var_dump(  ']); die();
  $service = new TypeServiceManager(getConnection());
  $bool = $service->Modifier(new TypeService($_POST));
  if ($bool) {
    header('location:../view/admin/typeService.php');
  }
} 

  if (isset($_POST['enregistrerTypeService'])) {
    if (isset($_POST['libelle']) && !empty($_POST['libelle']) && isset($_POST['description'])) {
      $service = new TypeServiceManager(getConnection());
      $bool = $service->Ajouter(new TypeService($_POST));
      if ($bool) {
        header('location:../view/admin/typeService.php?ajouter=1');
      }
    }
  }


if (isset($_GET['idModifierType']) && !empty($_GET['idModifierType'])) {
  if ($_GET['etatType'] == "0") {
    header('location:../view/admin/typeService.php');
  } else {
    $serviceManager = new TypeServiceManager(getConnection());
    $service = $serviceManager->SelectTypeService($_GET['idModifierType']);


    if ($service) {
      $_SESSION['libelle'] = $service->getLibelle();
      $_SESSION['description'] = $service->getDescription();
      $_SESSION['idType'] = $service->getId();

      header('location:../view/admin/typeService.php?modifierType=1');
    }
  }
}
if (isset($_GET['idDesactiverType']) && !empty($_GET['idDesactiverType'])) {
  $serviceManager = new TypeServiceManager(getConnection());
  $bool = $serviceManager->Desactiver($_GET['idDesactiverType']);
  if ($bool) {
    header('location:../view/admin/typeService.php');
  }
}

if (isset($_GET['idActiverType']) && !empty($_GET['idActiverType'])) {
  $serviceManager = new TypeServiceManager(getConnection());
 
  $bool = $serviceManager->Activer($_GET['idActiverType']);
  //  var_dump("valeur");die();

  if ($bool) {
    header('location:../view/admin/typeService.php');
  }
}

if (isset($_POST['enregistrerService']) && $_SESSION['modifierSer'] == "0") { ////////////////////:/
  if (
    isset($_POST['libelle']) && !empty($_POST['libelle'])
    && isset($_POST['prix']) && !empty($_POST['prix'])
  ) {
 
    $_SESSION['modifierSer'] =  0; 
    $bd = getConnection(); //enregistrer imag
    $requete = $bd->prepare('INSERT INTO image(nom, taille, type, bin) VALUES(?,?,?,?)');
    $requete->execute(array(
      $_FILES['image']['name'],
      $_FILES['image']['size'],
      $_FILES['image']['type'],
      file_get_contents($_FILES['image']['tmp_name'])
    ));
    $id = $bd->lastInsertId();
    

      // var_dump($_POST['prix']); die();
    $manager = new ServiceManager(getConnection());
    $bool = $manager->Ajouter(new Service(array(
      'libelle' => $_POST['libelle'],
      'prix' => $_POST['prix'],
      'description' => $_POST['description'],
      'image' => $id,
      'idTypeService' => $_POST['idTypeService'],
      'idAdministrateur' => $_SESSION['idPersonne']
    )));
    if ($bool) {
      header('location:../view/admin/service.php');
    }
  }
 
}
if(isset($_POST['enregistrerEntreprise'])){
  if(isset($_POST['nomEntreprise']) && isset($_POST['description']) && isset($_POST['telephone']) && isset($_POST['adresse'])){


    $bd = getConnection(); //enregistrer imag
    $requete = $bd->prepare('INSERT INTO image(nom, taille, type, bin) VALUES(?,?,?,?)');
    $requete->execute(array(
      $_FILES['logo']['name'],
      $_FILES['logo']['size'],
      $_FILES['logo']['type'],
      file_get_contents($_FILES['logo']['tmp_name'])
    ));
    $id = $bd->lastInsertId();


    $entrepriseManager = new EntrepriseManager($bd);
    if($entrepriseManager->CreerEntreprise(new Entreprise(array(
      'nomEntreprise' => $_POST['nomEntreprise'],
      'adresse' => $_POST['adresse'],
      'telephone' => $_POST['telephone'],
      'description' => $_POST['description'],
      'idAdministrateur' => $_SESSION['idPersonne'],
      'logo' => $id
    )))){
      header('location:../view/admin/fichierIndex.php');
    } 
  }
}
if(isset($_GET['entreprise'])){
  $requete = $bd->query("SELECT * FROM Entreprise");
   $entreprise = new Entreprise($requete->fetch(PDO::FETCH_ASSOC));
   $_SESSION['nomEntreprise'] = $entreprise->getNomEntreprise();
   $_SESSION['telephone'] = $entreprise->getTelephone();
   $_SESSION['adresse'] =$entreprise->getAdresse();
   $_SESSION['descriptionEntreprise'] = $entreprise->getDescription();
   $_SESSION['logo'] = $entreprise->getLogo();

   header('location:../view/admin/fichierIndex.php?chargerEntreprise=ok');

}
if(isset($_POST['ModifierEntreprise'])){
  // var_dump("valeur"); die();
  if(isset($_POST['nomEntreprise']) && isset($_POST['description']) && isset($_POST['telephone']) && isset($_POST['adresse'])){
   $bd = getConnection();
    if(isset($_GET['valeur']) && $_GET['valeur'] == "1"){
       
      try{
        $requete = $bd->prepare('UPDATE image SET nom = ?, taille = ?, type = ?, bin = ? WHERE id = ?');
        $requete->execute(array(
          $_FILES['logo']['name'],
          $_FILES['logo']['size'],
          $_FILES['logo']['type'],
          file_get_contents($_FILES['logo']['tmp_name']),
          $_SESSION['logo']
        ));
        $requete->closeCursor();
      }catch(Exception $ex){
        var_dump($ex->getMessage()); die();
      }
   }

    try{
      $requete = $bd->prepare("UPDATE Entreprise SET nomEntreprise = :nom, description = :desc, adresse = :ad, telephone = :tel");
      $requete->execute(array(
        ':nom' => $_POST['nomEntreprise'],
        ':desc' => $_POST['description'],
        ':ad' => $_POST['adresse'],
        ':tel' => $_POST['telephone']

      ));
    }catch(Exception $ex){
      var_dump($ex->getMessage()); die();
    }
   header('location:../view/admin/fichierIndex.php');

  }
}

if(isset($_POST['envoyer'])){

  if(isset($_POST['nomPrenom']) && isset($_POST['telephone']) && isset($_POST['email']) && isset($_POST['message'])){
    $admin = new AdministrateurManager(getConnection());

    if ($admin->AjouterContactUtilisateur(new Administrateur($_POST))) {
      header('location:../index.php?ajouterSms=1');
    }
  }
}
if(isset($_GET['idDesactiverContact']) && $_GET['idDesactiverContact'] != "0"){
    $adminManager = new AdministrateurManager(getConnection());
    $adminManager->SupprimerContact($_GET['idDesactiverContact']);
    header('location:../view/admin/admin.php');

    
}